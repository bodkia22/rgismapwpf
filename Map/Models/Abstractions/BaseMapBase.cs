﻿using Esri.ArcGISRuntime.Mapping;

namespace Map.Models.Abstractions
{
    public abstract class BaseMapBase
    {
        public string Name { get; set; }
        public Basemap BaseMapInstance { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}
