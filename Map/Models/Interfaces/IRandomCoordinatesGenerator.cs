﻿using System.Collections.Generic;
using Esri.ArcGISRuntime.Geometry;
using Map.Models.Abstractions;

namespace Map.Models.Interfaces
{
    public interface IRandomCoordinatesGenerator
    {
        List<MapPoint> GenerateForCity(City city, int numberOfPoints);
    }
}
