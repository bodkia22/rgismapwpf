﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using Esri.ArcGISRuntime.Geometry;
using Map.Models.Abstractions;

namespace Map.Models.Interfaces
{
    public interface IGraphicItemsManager
    {
        void PlotCityPoints(City city);
        void AddOrUpdateTextLabelForCity(City city, int numberOfLogos);
        void DeleteAllLogos();
        void DeleteAllLogosAtCity(City city);
        bool IsMapHasLogos();
        void DeleteSelectedLogo();
        bool IsMapHasSelectedLogo();
        event EventHandler DeleteCalloutFromMap;
    }
}
