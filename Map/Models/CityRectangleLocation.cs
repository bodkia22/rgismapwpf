﻿using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Mapping;

namespace Map.Models
{
    public class CityRectangleLocation
    {
        public double MinLatitude { get; set; }
        public double MaxLatitude { get; set; }
        public double MinLongitude { get; set; }
        public double MaxLongitude { get; set; }
        private Viewpoint CityRectangleLocationViewpoint { get; set; }

        public CityRectangleLocation(double minLatitude, double maxLatitude, double minLongitude, double maxLongitude)
        {
            MinLatitude = minLatitude;
            MaxLatitude = maxLatitude;
            MinLongitude = minLongitude;
            MaxLongitude = maxLongitude;

            CityRectangleLocationViewpoint = new Viewpoint(new Envelope(minLongitude, minLatitude, maxLongitude, maxLatitude, SpatialReferences.Wgs84));
        }

        public Viewpoint GetCityCoordinates() => CityRectangleLocationViewpoint;

        public MapPoint GetCenterPoint()
        {
            var longitude = (MinLongitude + MaxLongitude) / 2;
            var latitude = (MinLatitude + MaxLatitude) / 2;

            return new MapPoint(longitude, latitude, SpatialReferences.Wgs84);
        }

        public double GetLatitude => (MinLatitude + MaxLatitude) / 2;

        public double GetLongitude => (MinLongitude + MaxLongitude) / 2;
    }
}
