﻿using Esri.ArcGISRuntime.Mapping;
using Map.Models.Abstractions;

namespace Map.Models
{
    class StreetsVector : BaseMapBase
    {
        public StreetsVector()
        {
            Name = "StreetsVector";
            BaseMapInstance = Basemap.CreateStreetsVector();
        }
    }
}
