﻿using Esri.ArcGISRuntime.Mapping;
using Map.Models.Abstractions;

namespace Map.Models
{
    class DarkGrayCanvasVector : BaseMapBase
    {
        public DarkGrayCanvasVector()
        {
            Name = "DarkGrayCanvasVector";
            BaseMapInstance = Basemap.CreateDarkGrayCanvasVector();
        }
    }
}
