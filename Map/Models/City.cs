﻿using System.Collections.Generic;
using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Symbology;
using Esri.ArcGISRuntime.UI;

namespace Map.Models
{
    public class City
    {
        public string Name { get; set; }
        public CityRectangleLocation Location { get; set; }

        public List<Graphic> Graphics { get; set; } = new List<Graphic>();

        public void UpdateCityGraphics(List<MapPoint> mapPoint, FeatureLogo featureLogoObject)
        {
            var picMarkerSym = new PictureMarkerSymbol(featureLogoObject.BitmapImage.UriSource);

            foreach (var point in mapPoint)
            {
                Graphic graphic = new Graphic(point, picMarkerSym);
                graphic.Attributes.Add($"{featureLogoObject.Descriptions}", null);
                Graphics.Add(graphic);
            }
        }

        public bool IsCityHasLogos() => Graphics.Count > 0;

        public override string ToString()
        {
            return Name;
        }
    }
}
