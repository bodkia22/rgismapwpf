﻿using System;
using System.Collections.Generic;
using Esri.ArcGISRuntime.Geometry;
using Map.Models.Abstractions;
using Map.Models.Interfaces;

namespace Map.Models.Services
{
    public class RandomCoordinatesGenerator : IRandomCoordinatesGenerator
    {
        private readonly Random _random = new Random();
        public List<MapPoint> GenerateForCity(City city, int numberOfPoints)
        {
            List<MapPoint> points = new List<MapPoint>();

            for (int i = 0; i < numberOfPoints; i++)
            {
                var x = GetRandomNumber(city.Location.MinLongitude, city.Location.MaxLongitude);
                var y = GetRandomNumber(city.Location.MinLatitude, city.Location.MaxLatitude);

                points.Add(new MapPoint(x, y, SpatialReferences.Wgs84));
            }

            return points;
        }

        private double GetRandomNumber(double minimum, double maximum)
        {
            return _random.NextDouble() * (maximum - minimum) + minimum;
        }
    }
}
