﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Media.Imaging;
using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Symbology;
using Esri.ArcGISRuntime.UI;
using Esri.ArcGISRuntime.UI.Controls;
using Map.Models.Abstractions;
using Map.Models.Interfaces;

namespace Map.Models
{
    public class GraphicItemsManager : IGraphicItemsManager
    {
        private readonly GraphicsOverlay _pictureMarkerGraphicsOverlay;
        private readonly GraphicsOverlay _textSymbolGraphicsOverlay;

        public GraphicItemsManager(GraphicsOverlay pictureMarkerGraphicsOverlay, GraphicsOverlay textSymbolGraphicsOverlay)
        {
            _pictureMarkerGraphicsOverlay = pictureMarkerGraphicsOverlay;
            _textSymbolGraphicsOverlay = textSymbolGraphicsOverlay;
        }

        public void PlotCityPoints(City city)
        {
            foreach (var graphic in city.Graphics)
            {
                if (!_pictureMarkerGraphicsOverlay.Graphics.Contains(graphic))
                {
                    _pictureMarkerGraphicsOverlay.Graphics.Add(graphic);
                }
            }
        }
        
        public void AddOrUpdateTextLabelForCity(City city, int numberOfLogos)
        {
            var mapPoint = city.Location.GetCenterPoint();

            var textSym = new TextSymbol
            {
                Color = Color.Red,
                Size = 50,
                Text = $"{numberOfLogos}",
                FontWeight = FontWeight.Bold
            };

            var graphic = new Graphic(mapPoint, textSym);
            var oldGraphic = _textSymbolGraphicsOverlay.Graphics.FirstOrDefault(x => x.Geometry.Extent.XMax == mapPoint.X &&
                                                                             x.Geometry.Extent.YMax == mapPoint.Y);
            _textSymbolGraphicsOverlay.Graphics.Remove(oldGraphic);
            if (numberOfLogos != 0)
            {
                _textSymbolGraphicsOverlay.Graphics.Add(graphic);
            }
        }

        public void DeleteAllLogos()
        {
            DeleteCalloutFromMap?.Invoke(this, EventArgs.Empty);

            _pictureMarkerGraphicsOverlay.Graphics.Clear();
            _textSymbolGraphicsOverlay.Graphics.Clear();
        }

        public void DeleteAllLogosAtCity(City city)
        {
            var res = city.Graphics;

            foreach (var graphic in res)
            {
                _pictureMarkerGraphicsOverlay.Graphics.Remove(graphic);
            }

            var textGraphic = _textSymbolGraphicsOverlay.Graphics.FirstOrDefault(x => x.Geometry.Extent.XMax == city.Location.GetLongitude &&
                x.Geometry.Extent.YMax == city.Location.GetLatitude);

            _textSymbolGraphicsOverlay.Graphics.Remove(textGraphic);

            DeleteCalloutFromMap?.Invoke(this, EventArgs.Empty);
        }

        public void DeleteSelectedLogo() 
        {
            var graphic = _pictureMarkerGraphicsOverlay.SelectedGraphics.FirstOrDefault();

            _pictureMarkerGraphicsOverlay.Graphics.Remove(graphic);

            DeleteCalloutFromMap?.Invoke(this, EventArgs.Empty);
        }

        public bool IsMapHasLogos() => _pictureMarkerGraphicsOverlay.Graphics.Count > 0;

        public bool IsMapHasSelectedLogo() => _pictureMarkerGraphicsOverlay.SelectedGraphics.Any();
        
        public event EventHandler DeleteCalloutFromMap;
    }
}
