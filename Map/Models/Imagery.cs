﻿using Esri.ArcGISRuntime.Mapping;
using Map.Models.Abstractions;

namespace Map.Models
{
    class Imagery : BaseMapBase
    {
        public Imagery()
        {
            Name = "Imagery";
            BaseMapInstance = Basemap.CreateImagery();
        }
    }
}
