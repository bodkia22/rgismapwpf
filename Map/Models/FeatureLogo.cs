﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Esri.ArcGISRuntime.Symbology;
using Map.Annotations;
using Map.Models.Abstractions;

namespace Map.Models
{
    public class FeatureLogo 
    {
        public BitmapImage BitmapImage { get; set; }
        public string Descriptions { get; set; }
    }
}
