﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Map.Models
{
    public class ViewPointError 
    {
        public bool LatitudeHasError { get; set; }
        public bool LongitudeHasError { get; set; }
        public bool ScaleHasError { get; set; }

        public ViewPointError(bool latitudeHasError, bool longitudeHasError, bool scaleHasError)
        {
            LatitudeHasError = latitudeHasError;
            LongitudeHasError = longitudeHasError;
            ScaleHasError = scaleHasError;
        }
        public bool HasError => LatitudeHasError || LongitudeHasError || ScaleHasError;
    }
}
