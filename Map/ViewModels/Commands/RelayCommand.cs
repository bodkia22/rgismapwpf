﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Map.ViewModels.Commands
{
    public class RelayCommand : ICommand
    {
        #region Data Members
        private readonly Action<object> _execute;
        private readonly Predicate<object> _canExecute;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="execute">Delegate that will be executed when the command is invoked</param>
        public RelayCommand(Action<object> execute) : this(execute, null) { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="execute">Delegate that will be executed when the command is invoked</param>
        /// <param name="canExecute">Method that defines whether the command can be executed</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        #endregion Constructors

        #region Methods
        /// <summary>
        /// Raises the command's CanExecuteChanged event in the UI thread (unless in unit tests)
        /// </summary>
        public void RaiseCanExecuteChanged()
        {
            if (CanExecuteChanged != null)
            {
                if (Application.Current != null)
                {
                    Application.Current.Dispatcher.InvokeAsync(() => CanExecuteChanged(this, EventArgs.Empty));
                }
                else
                {
                    CanExecuteChanged(this, EventArgs.Empty);
                }
            }
        }

        #endregion
        //public event EventHandler CanExecuteChanged
        //{
        //    add { CommandManager.RequerySuggested += value; }
        //    remove { CommandManager.RequerySuggested -= value; }
        //}

        #region ICommand
        public bool CanExecute(object parameter)
        {
            var res = _canExecute?.Invoke(parameter) ?? true;
            return res;
        }

        public event EventHandler CanExecuteChanged;
        public void Execute(object parameter)
        {
            _execute(parameter);
        }

        #endregion ICommand
    }
}
