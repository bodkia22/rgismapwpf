﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Media.Imaging;
using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Mapping;
using MahApps.Metro.Controls.Dialogs;
using Map.Models;
using Map.Models.Abstractions;
using Map.Models.Interfaces;
using Map.ViewModels.Commands;
using Notifications.Wpf;
using Notifications.Wpf.Controls;

namespace Map.ViewModels
{
    /// <summary>
    /// Provides map data to an application
    /// </summary>
    //
    public class MapViewModel : INotifyPropertyChanged
    {
        private readonly IRandomCoordinatesGenerator _randomCoordinatesGenerator;
        private readonly IGraphicItemsManager _graphicItemsManager;
        private readonly IDialogCoordinator _dialogCoordinator;
        private readonly INotificationManager _notificationManager;

        private LogoViewModel _logoViewModel;
        public LogoViewModel LogoViewModel
        {
            get => _logoViewModel;
            set { _logoViewModel = value; OnPropertyChanged(); }
        }

        private Esri.ArcGISRuntime.Mapping.Map _map;
        public Esri.ArcGISRuntime.Mapping.Map Map
        {
            get => _map;
            set { _map = value; OnPropertyChanged(); }
        }

        private ViewPointViewModel _viewPointViewModel;
        public ViewPointViewModel ViewPointViewModel
        {
            get => _viewPointViewModel;
            set { _viewPointViewModel = value; OnPropertyChanged(); }
        }

        private Viewpoint _currentViewPoint;
        public Viewpoint CurrentViewPoint
        {
            get => _currentViewPoint;
            set { _currentViewPoint = value; OnPropertyChanged(); }
        }

        private bool _isMapItemSelected;
        public bool IsMapItemSelected
        {
            get => _isMapItemSelected;
            set { _isMapItemSelected = value; OnPropertyChanged();}
        }

        public MapViewModel(IRandomCoordinatesGenerator randomCoordinatesGenerator, IGraphicItemsManager graphicItemsManager, IDialogCoordinator dialogCoordinator, INotificationManager notificationManager)
        {
            var startCity = new City{ Name = "New York",Location = new CityRectangleLocation(40.5806, 40.9367, -74.181215, -73.752749)};
            var defaultLogo = new FeatureLogo
            {
                BitmapImage =
                    new BitmapImage(new Uri("pack://application:,,,/Infrastructure;component/Images/llama.png")),
                Descriptions = "Beautiful lama"
            };

            _randomCoordinatesGenerator = randomCoordinatesGenerator;
            _graphicItemsManager = graphicItemsManager;
            _dialogCoordinator = dialogCoordinator;
            _notificationManager = notificationManager;

            Map = new Esri.ArcGISRuntime.Mapping.Map(Basemap.CreateImagery());
            ViewPointViewModel = new ViewPointViewModel(startCity.Location.GetCenterPoint().X,
                startCity.Location.GetCenterPoint().Y, 200000);

            LogoViewModel = new LogoViewModel(startCity, defaultLogo);

            ViewPointViewModel.PropertyChanged += ViewPointViewModelOnPropertyChanged;
            LogoViewModel.PropertyChanged += LogoViewModelOnPropertyChanged;

            BaseMapViewList = new ObservableCollection<BaseMapBase>
            {
                new Imagery(),
                new DarkGrayCanvasVector(),
                new StreetsVector()
            };

            CityList = new ObservableCollection<City>
            {
                startCity,
                new City{Name = "Rivne",Location = new CityRectangleLocation(50.598,50.644,26.2277,26.2935)},
                new City{Name = "Lviv",Location = new CityRectangleLocation(49.7995,49.871,23.9750,24.0827)},
                new City{Name = "London",Location = new CityRectangleLocation(51.2654,51.6875,-0.5059, 0.2675)},
                new City{Name = "Paris",Location = new CityRectangleLocation(48.817146,48.901,2.255751,2.4138)},
            };

            LogoList = new ObservableCollection<FeatureLogo>
            {
                defaultLogo,
                new FeatureLogo{BitmapImage = new BitmapImage(new Uri("pack://application:,,,/Infrastructure;component/Images/exposure.png")), Descriptions = "Great weather"},
                new FeatureLogo{BitmapImage = new BitmapImage(new Uri("pack://application:,,,/Infrastructure;component/Images/helmet.png")), Descriptions = "Heavy helmet"},
                new FeatureLogo{BitmapImage = new BitmapImage(new Uri("pack://application:,,,/Infrastructure;component/Images/asterisk.png")), Descriptions = "Asterisk"}
            };

            ChangeBaseMapCommand = new RelayCommand(ChangeBaseMap);
            ZoomToLocationCommand = new RelayCommand(ZoomToLocation, CanZoomToLocation);
            ZoomToCityCommand = new RelayCommand(ZoomToCity);
            ScalePlusCommand = new RelayCommand(ScalePlus);
            ScaleMinusCommand = new RelayCommand(ScaleMinus);
            AddFeatureCommand = new RelayCommand(AddFeature, CanAddFeature);
            DeleteAllLogosCommand = new RelayCommand(DeleteAllLogos, CanDeleteAllLogosInMap);
            DeleteAllLogosInCityCommand = new RelayCommand(DeleteAllLogosInCity, CanDeleteAllLogosInCity);
            ShowMapLogoRemovalConfirmationCommand = new RelayCommand(ShowDialogAndDeleteSelectedLogo, CanShowDialogAndDeleteSelectedLogo);

            _map.InitialViewpoint =
                new Viewpoint(
                    new MapPoint(ViewPointViewModel.CurrentLongitudeValue, ViewPointViewModel.CurrentLatitudeValue,
                        SpatialReferences.Wgs84), ViewPointViewModel.CurrentScaleValue);

            TrashBoxLogo =
                new BitmapImage(new Uri("pack://application:,,,/Infrastructure;component/Images/trash-box.png"));
        }

        public ObservableCollection<BaseMapBase> BaseMapViewList { get; }
        public ObservableCollection<City> CityList { get; }
        public ObservableCollection<FeatureLogo> LogoList { get; }
        public BitmapImage TrashBoxLogo { get; }

        public RelayCommand ChangeBaseMapCommand { get; }
        public RelayCommand ZoomToLocationCommand { get; }
        public RelayCommand ZoomToCityCommand { get; }
        public RelayCommand ScalePlusCommand { get; }
        public RelayCommand ScaleMinusCommand { get; }
        public RelayCommand DeleteAllLogosCommand { get; }
        public RelayCommand DeleteAllLogosInCityCommand { get; }
        public RelayCommand AddFeatureCommand { get; }
        public RelayCommand ShowMapLogoRemovalConfirmationCommand { get; } 

        private void LogoViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            AddFeatureCommand.RaiseCanExecuteChanged();
            DeleteAllLogosInCityCommand.RaiseCanExecuteChanged();
        }

        private void ViewPointViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            ZoomToLocationCommand.RaiseCanExecuteChanged();
        }

        private void DeleteAllLogos(object object1)
        {
            foreach (var city in CityList)
            {
               city.Graphics.Clear();
            }

            _graphicItemsManager.DeleteAllLogos();

            _notificationManager.Show(
                new NotificationContent { Title = "Success", Message = "All items was successfully deleted", Type = NotificationType.Information },
                "WindowArea", TimeSpan.FromSeconds(3));

            DeleteAllLogosCommand.RaiseCanExecuteChanged();
            DeleteAllLogosInCityCommand.RaiseCanExecuteChanged();
        }

        private void DeleteAllLogosInCity(object object1)
        {
            _graphicItemsManager.DeleteAllLogosAtCity(LogoViewModel.City);
            LogoViewModel.City.Graphics.Clear();

            _notificationManager.Show(
                new NotificationContent { Title = "Success", Message = $"All items in {LogoViewModel.City} was successfully deleted", Type = NotificationType.Information },
                "WindowArea", TimeSpan.FromSeconds(3));

            DeleteAllLogosCommand.RaiseCanExecuteChanged();
            DeleteAllLogosInCityCommand.RaiseCanExecuteChanged();
        }

        private void ZoomToCity(object cityObject)
        {
            var city = (City) cityObject;
            var coordinates = city.Location.GetCityCoordinates();
            CurrentViewPoint = coordinates;

            LogoViewModel.City = city;

            ViewPointViewModel.ViewPointError = new ViewPointError(true, false, false);
        }

        private void ZoomToLocation(object object1)
        {
            MapPoint mp = new MapPoint(ViewPointViewModel.CurrentLongitudeValue, ViewPointViewModel.CurrentLatitudeValue, SpatialReferences.Wgs84);
            Viewpoint newViewPoint = new Viewpoint(mp,
                ViewPointViewModel.CurrentScaleValue);

            CurrentViewPoint = newViewPoint;
        }

        private void ChangeBaseMap(object mapObject)
        {
            var mapName = (BaseMapBase)mapObject;
            Map.Basemap = mapName.BaseMapInstance;
        }

        private void ScalePlus(object object1)
        {
            ViewPointViewModel.CurrentScaleValue -= 30000;

            CurrentViewPoint = new Viewpoint(
                new MapPoint(ViewPointViewModel.CurrentLongitudeValue, ViewPointViewModel.CurrentLatitudeValue,
                    SpatialReferences.Wgs84), ViewPointViewModel.CurrentScaleValue);
        }

        private void ScaleMinus(object object1)
        {
            ViewPointViewModel.CurrentScaleValue += 30000;

            CurrentViewPoint = new Viewpoint(
                new MapPoint(ViewPointViewModel.CurrentLongitudeValue, ViewPointViewModel.CurrentLatitudeValue,
                    SpatialReferences.Wgs84), ViewPointViewModel.CurrentScaleValue);
        }

        private void AddFeature(object object1)
        {
            var mapPointList = _randomCoordinatesGenerator.GenerateForCity(LogoViewModel.City, LogoViewModel.NumberOfLogos);

            LogoViewModel.City.UpdateCityGraphics(mapPointList, LogoViewModel.Logo);
            _graphicItemsManager.PlotCityPoints(LogoViewModel.City);
            _graphicItemsManager.AddOrUpdateTextLabelForCity(LogoViewModel.City, LogoViewModel.City.Graphics.Count);

            _notificationManager.Show(
                new NotificationContent { Title = "Success", Message = $"{LogoViewModel.NumberOfLogos} items was successfully added", Type = NotificationType.Success },
                "WindowArea", TimeSpan.FromSeconds(3));

            DeleteAllLogosCommand.RaiseCanExecuteChanged();
            DeleteAllLogosInCityCommand.RaiseCanExecuteChanged();
        }

        private async void ShowDialogAndDeleteSelectedLogo(object object1)
        {
            var res = await _dialogCoordinator.ShowMessageAsync(this,"Are you sure, you want to remove this logo ?", null, MessageDialogStyle.AffirmativeAndNegative);

            if (res == MessageDialogResult.Affirmative)
            {
                _graphicItemsManager.DeleteSelectedLogo();

                foreach (var city in CityList)
                {
                    var selectedGraphic = city.Graphics.FirstOrDefault(x => x.IsSelected);
                    if (selectedGraphic != null)
                    {
                        city.Graphics.Remove(selectedGraphic);
                        _graphicItemsManager.AddOrUpdateTextLabelForCity(city, city.Graphics.Count); //set selected city here
                    }
                }
                _notificationManager.Show(
                new NotificationContent { Title = "Success", Message = "Item was successfully deleted", Type = NotificationType.Success},
                "WindowArea", TimeSpan.FromSeconds(3));
            }

            ShowMapLogoRemovalConfirmationCommand.RaiseCanExecuteChanged();
        }

        private bool CanDeleteAllLogosInCity(object object1)
        {
            return LogoViewModel.City.IsCityHasLogos();
        }

        private bool CanDeleteAllLogosInMap(object object1)
        {
            return _graphicItemsManager.IsMapHasLogos();
        }

        private bool CanAddFeature(object object1) => LogoViewModel.NumberOfLogos > 0 && LogoViewModel.NumberOfLogosHasValidationError == false;

        private bool CanZoomToLocation(object object1) => ViewPointViewModel.Error == null && ViewPointViewModel.ViewPointError.HasError == false;

        private bool CanShowDialogAndDeleteSelectedLogo(object object1)
        {
            IsMapItemSelected = _graphicItemsManager.IsMapHasSelectedLogo();
            return IsMapItemSelected;
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var propertyChangedHandler = PropertyChanged;
            if (propertyChangedHandler != null)
                propertyChangedHandler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
