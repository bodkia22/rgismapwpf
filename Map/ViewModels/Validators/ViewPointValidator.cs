﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Contracts;
using System.Linq;
namespace Map.ViewModels.Validators
{
    public class ViewPointValidator : IDataErrorInfo
    {
        List<ValidationResult> validationResults = new List<ValidationResult>();

        public string this[string columnName]
        {
            get
            {
                if (validationResults.Count > 0)
                {
                    validationResults.Clear();
                }

                var property = GetType().GetProperty(columnName);
                Contract.Assert(null != property);

                var validationContext = new ValidationContext(this)
                {
                    MemberName = columnName
                };

                var isValid = Validator.TryValidateProperty(property.GetValue(this), validationContext, validationResults);
                if (isValid)
                {
                    return null;
                }

                return validationResults.First().ErrorMessage;
            }
        }

        public string Error
        {
            get
            {
                if (validationResults.Count == 0)
                {
                    return null;
                }
                return validationResults.First().ErrorMessage;
            }
        }
    }
}
