﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Map.DependencyObject;

namespace Map.ViewModels.Validators
{
    public class StringToIntValidationRule : ValidationRule
    {
        public NumberOfLogosErrorDependencyProperty NumberOfLogos { get; set; }
        
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var stringNumber = value.ToString();

            if (int.TryParse(stringNumber, NumberStyles.Integer, CultureInfo.GetCultureInfo("en-US"), out int result))
            {
                NumberOfLogos.NumberOfLogosHasError = false;
                return ValidationResult.ValidResult;
            }

            NumberOfLogos.NumberOfLogosHasError = true;
            return new ValidationResult(false, $"{value} is not valid integer");
        }
    }
}
