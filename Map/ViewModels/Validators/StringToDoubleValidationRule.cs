﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;
using MahApps.Metro.Controls;
using Map.DependencyObject;
using Map.Models;
using Map.ViewModels.Commands;

namespace Map.ViewModels.Validators
{
    public class StringToDoubleValidationRule : ValidationRule
    {
        private const string Latitude = "Latitude";
        private const string Longitude = "Longitude";
        private const string Scale = "Scale";

        public ZoomDependencyProperty Zoom { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo, BindingExpressionBase owner)
        {
            var k = owner.BindingGroup.Name;


            return base.Validate(value, cultureInfo, owner);
        }

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            var stringNumber = value.ToString();

            if (double.TryParse(stringNumber, NumberStyles.Float, CultureInfo.GetCultureInfo("en-US"), out double result))
            {
                if (Zoom.Point == Latitude)
                {
                    Zoom.ViewPointError =
                        new ViewPointError(false, Zoom.ViewPointError.LongitudeHasError, Zoom.ViewPointError.ScaleHasError);
                }
                if (Zoom.Point == Longitude)
                {
                    Zoom.ViewPointError =
                        new ViewPointError(Zoom.ViewPointError.LatitudeHasError, false, Zoom.ViewPointError.ScaleHasError);
                }
                if (Zoom.Point == Scale)
                {
                    Zoom.ViewPointError =
                        new ViewPointError(Zoom.ViewPointError.LatitudeHasError, Zoom.ViewPointError.LongitudeHasError, false);
                }

                return ValidationResult.ValidResult;
            }

            if (Zoom.Point == Latitude)
            {
                Zoom.ViewPointError =
                    new ViewPointError(true, Zoom.ViewPointError.LongitudeHasError, Zoom.ViewPointError.ScaleHasError);
            }
            if (Zoom.Point == Longitude)
            {
                Zoom.ViewPointError =
                    new ViewPointError(Zoom.ViewPointError.LatitudeHasError, true, Zoom.ViewPointError.ScaleHasError);
            }
            if (Zoom.Point == Scale)
            {
                Zoom.ViewPointError =
                    new ViewPointError(Zoom.ViewPointError.LatitudeHasError, Zoom.ViewPointError.LongitudeHasError, true);
            }

            return new ValidationResult(false, $"{value} is not valid numeric !");
        }
    }
}
