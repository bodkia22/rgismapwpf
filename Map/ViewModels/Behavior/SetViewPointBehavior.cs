﻿using System.Windows;
using System.Windows.Interactivity;
using Esri.ArcGISRuntime.Mapping;
using Esri.ArcGISRuntime.UI.Controls;

namespace Map.ViewModels.Behavior
{
    public class SetViewPointBehavior : Behavior<MapView>
    {
        //add dependency property
        public static readonly DependencyProperty MapViewpointProperty = DependencyProperty.
            Register("MapViewpoint", typeof(Viewpoint), typeof(SetViewPointBehavior));

        public Viewpoint MapViewpoint
        {
            get => (Viewpoint)GetValue(MapViewpointProperty);
            set { SetValue(MapViewpointProperty, value); }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Property.Name == MapViewpointProperty.Name)
            {
                AssociatedObject.SetViewpoint(MapViewpoint);
            }
        }
    }
}
