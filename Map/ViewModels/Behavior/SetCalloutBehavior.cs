﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interactivity;
using Esri.ArcGISRuntime.Data;
using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Mapping;
using Esri.ArcGISRuntime.Symbology;
using Esri.ArcGISRuntime.UI;
using Esri.ArcGISRuntime.UI.Controls;
using MahApps.Metro.Controls.Dialogs;
using Map.Models;
using Map.Models.Services;
using Map.ViewModels.Commands;

namespace Map.ViewModels.Behavior
{
    public class SetCalloutBehavior : Behavior<MapView>
    {
        public static readonly DependencyProperty ItemSelectedCommandProperty = DependencyProperty.
            Register("ItemSelectedCommand", typeof(RelayCommand), typeof(SetCalloutBehavior));

        public RelayCommand ItemSelectedCommand
        {
            get => (RelayCommand)GetValue(ItemSelectedCommandProperty);
            set { SetValue(ItemSelectedCommandProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            // Присоединение обработчиков событий       
            AssociatedObject.GeoViewTapped += AssociatedObject_GeoViewTapped;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            // Удаление обработчиков событий
            AssociatedObject.GeoViewTapped -= AssociatedObject_GeoViewTapped;
        }

        private async void AssociatedObject_GeoViewTapped(object sender, GeoViewInputEventArgs e)
        {
            double tolerance = 10d; // Use larger tolerance for touch
            int maximumResults = 1; // Only return one graphic  
            bool onlyReturnPopups = false; // Return more than popups

            var pictureMarkerGraphicsOverlay = AssociatedObject.GraphicsOverlays.FirstOrDefault(x => x.Graphics.All(gr => gr.Symbol is PictureMarkerSymbol));

            IdentifyGraphicsOverlayResult identifyResults = await AssociatedObject.IdentifyGraphicsOverlayAsync(
                pictureMarkerGraphicsOverlay,
                e.Position,
                tolerance,
                onlyReturnPopups,
                maximumResults);

            var graphic = identifyResults.Graphics.FirstOrDefault();
            
            if (graphic != null)
            {
                var symbol = graphic.Symbol as PictureMarkerSymbol;

                string specialText = graphic.Attributes.Keys.FirstOrDefault();
                
                // Format the display callout string based upon the projected map point (example: "Lat: 100.123, Long: 100.234")
                string mapLocationDescription = string.Format("Lat: {0:F3} Long: {1:F3}", graphic.Geometry.Extent.XMax, graphic.Geometry.Extent.YMax);

                // Create a new callout definition using the formatted string
                var myCalloutDefinition = new CalloutDefinition($"{specialText} location:", mapLocationDescription);
                myCalloutDefinition.LeaderOffsetY = 22;
                myCalloutDefinition.Icon = new RuntimeImage(symbol.Uri);
                myCalloutDefinition.ButtonImage = new RuntimeImage(new Uri("pack://application:,,,/Infrastructure;component/Images/close2.png"));
                myCalloutDefinition.OnButtonClick = UnselectFeature;
                
                // Display the callout
                AssociatedObject.ShowCalloutAt(graphic.Geometry.Extent.GetCenter(), myCalloutDefinition);

                pictureMarkerGraphicsOverlay.ClearSelection();
                //Display selected item
                var index = pictureMarkerGraphicsOverlay.Graphics.IndexOf(graphic);
                pictureMarkerGraphicsOverlay.Graphics[index].IsSelected = true;
            }
            ItemSelectedCommand.RaiseCanExecuteChanged();
        }

        private void UnselectFeature(object obj)
        {
            AssociatedObject.DismissCallout();
            AssociatedObject.GraphicsOverlays[0].ClearSelection();
            ItemSelectedCommand.RaiseCanExecuteChanged();
        }
    }
}
