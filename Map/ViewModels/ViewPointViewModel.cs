﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Map.Models;
using Map.ViewModels.Validators;

namespace Map.ViewModels
{
    public class ViewPointViewModel : ViewPointValidator, INotifyPropertyChanged
    {
        private double _currentLongitudeValue;
        [Required(ErrorMessage = "Longitude is required")]
        [Range(-180, 180, ErrorMessage = "Longitude must be between -180 and 180")]
        public double CurrentLongitudeValue
        {
            get => _currentLongitudeValue;
            set
            {
                _currentLongitudeValue = value;
                OnPropertyChanged();
            }
        }

        private double _currentLatitudeValue;
        [Required(ErrorMessage = "Latitude is required")]
        [Range(-90, 90, ErrorMessage = "Latitude must be between -90 and 90")]
        public double CurrentLatitudeValue
        {
            get => _currentLatitudeValue;
            set { _currentLatitudeValue = value; OnPropertyChanged(); }
        }

        private double _currentScaleValue;
        [Required(ErrorMessage = "Scale is required")]
        [Range(1, 5000000, ErrorMessage = "Scale must be between 0 and 5000000")]
        public double CurrentScaleValue
        {
            get => _currentScaleValue;
            set { _currentScaleValue = value; OnPropertyChanged(); }
        }

        private ViewPointError _viewPointError;

        public ViewPointError ViewPointError
        {
            get => _viewPointError;
            set { _viewPointError = value; OnPropertyChanged(); }
        }

        public ViewPointViewModel(double longitude, double latitude, double scale)
        {
            CurrentLongitudeValue = longitude;
            CurrentLatitudeValue = latitude;
            CurrentScaleValue = scale;

            ViewPointError = new ViewPointError(false, false, false);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
