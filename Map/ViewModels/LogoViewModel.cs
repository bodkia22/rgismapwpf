﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media.Imaging;
using Map.Models;
using Map.Models.Abstractions;


namespace Map.ViewModels
{
    public class LogoViewModel : INotifyPropertyChanged
    {
        private FeatureLogo _logo;

        public FeatureLogo Logo
        {
            get => _logo;
            set { _logo = value; OnPropertyChanged(); }
        }

        private City _city;

        public City City
        {
            get => _city;
            set { _city = value; OnPropertyChanged(); }
        }

        private int _numberOfLogos;

        public int NumberOfLogos
        {
            get => _numberOfLogos;
            set { _numberOfLogos = value; OnPropertyChanged(); }
        }

        private bool _numberOfLogosHasValidationError;

        public bool NumberOfLogosHasValidationError
        {
            get => _numberOfLogosHasValidationError;
            set { _numberOfLogosHasValidationError = value; OnPropertyChanged(); }
        }
        public LogoViewModel(City startCity, FeatureLogo defaultImage)
        {
            City = startCity;
            Logo = defaultImage;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}