﻿using Esri.ArcGISRuntime.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Map.Models;
using Map.Models.Services;
using Map.ViewModels;
using Notifications.Wpf;

namespace Map
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            var graphicsOverlayForLogo = new GraphicsOverlay();

            var graphicsOverlayForText = new GraphicsOverlay();

            MyMapView.GraphicsOverlays.Add(graphicsOverlayForLogo);
            MyMapView.GraphicsOverlays.Add(graphicsOverlayForText);

            GraphicItemsManager graphicItemsManager = new GraphicItemsManager(graphicsOverlayForLogo, graphicsOverlayForText);

            var MapViewModel = new MapViewModel(new RandomCoordinatesGenerator(), graphicItemsManager,
                DialogCoordinator.Instance, new NotificationManager());

            DataContext = MapViewModel;

            graphicItemsManager.DeleteCalloutFromMap += delegate { MyMapView.DismissCallout(); };
        }
    }
}
