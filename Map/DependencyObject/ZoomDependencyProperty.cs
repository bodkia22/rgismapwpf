﻿using System.Windows;
using Map.Models;

namespace Map.DependencyObject
{
    public class ZoomDependencyProperty : System.Windows.DependencyObject
    {
        public ViewPointError ViewPointError
        {
            get => (ViewPointError)GetValue(ViewPointErrorProperty);
            set => SetValue(ViewPointErrorProperty, value);
        }

        public static readonly DependencyProperty ViewPointErrorProperty =
            DependencyProperty.Register("ViewPointError", typeof(ViewPointError), typeof(ZoomDependencyProperty));

        public string Point { get; set; }
    }
}
