﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Map.DependencyObject
{
    public class NumberOfLogosErrorDependencyProperty : System.Windows.DependencyObject
    {
        public bool NumberOfLogosHasError
        {
            get => (bool)GetValue(NumberOfLogosErrorProperty);
            set { SetValue(NumberOfLogosErrorProperty, value);}
        }
        public static readonly DependencyProperty NumberOfLogosErrorProperty =
            DependencyProperty.Register("NumberOfLogosHasError", typeof(bool),typeof(NumberOfLogosErrorDependencyProperty));
    }
}
