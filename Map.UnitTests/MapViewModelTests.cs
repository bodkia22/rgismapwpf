﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using Castle.Components.DictionaryAdapter;
using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Mapping;
using Esri.ArcGISRuntime.UI;
using MahApps.Metro.Controls.Dialogs;
using Map.Models;
using Map.Models.Abstractions;
using Map.Models.Interfaces;
using Map.UnitTests.Stub;
using Map.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Notifications.Wpf;

namespace Map.UnitTests
{
	[TestClass]
    public class MapViewModelTests
    {
        [TestMethod]
        public void DeleteAllLogosCommand_Execute_IsDeleteLogosCalled()
        {
            //Arrange
            var graphicItemsManagerMock = new Mock<IGraphicItemsManager>();
            var randomCoordinateGeneratorMock = new Mock<IRandomCoordinatesGenerator>();
            var dialogCoordinatorMock = new Mock<IDialogCoordinator>();
            var notificationManagerMock = new Mock<INotificationManager>();

            var mapViewmodel = new MapViewModel(randomCoordinateGeneratorMock.Object, graphicItemsManagerMock.Object, dialogCoordinatorMock.Object, notificationManagerMock.Object); 

            //Act
            mapViewmodel.DeleteAllLogosCommand.Execute(null);

            //Assert
            graphicItemsManagerMock.Verify(x => x.DeleteAllLogos(), Times.Once);
        }

        [TestMethod]
        public void DeleteAllLogosCommand_Execute_NotificationWasShowedOnes()
        {
            //Arrange
            var graphicItemsManagerMock = new Mock<IGraphicItemsManager>();
            var randomCoordinateGeneratorMock = new Mock<IRandomCoordinatesGenerator>();
            var dialogCoordinatorMock = new Mock<IDialogCoordinator>();
            var notificationManagerMock = new Mock<INotificationManager>();

            var mapViewmodel = new MapViewModel(randomCoordinateGeneratorMock.Object, graphicItemsManagerMock.Object, dialogCoordinatorMock.Object, notificationManagerMock.Object);

            //Act
            mapViewmodel.DeleteAllLogosCommand.Execute(null);

            //Assert
            notificationManagerMock.Verify(x => x.Show(It.IsAny<NotificationContent>(),
                It.IsAny<string>(),
                It.IsAny<TimeSpan>(), null, null), Times.Once);
        }

        [TestMethod]
        public void DeleteAllLogosInCity_Execute_IsDeleteAllLogosInCityCalled()
        {
            //Arrange
            var graphicItemsManagerMock = new Mock<IGraphicItemsManager>();
            var randomCoordinateGeneratorMock = new Mock<IRandomCoordinatesGenerator>();
            var notificationManagerMock = new Mock<INotificationManager>();

            var mapViewModel = new MapViewModel(randomCoordinateGeneratorMock.Object, graphicItemsManagerMock.Object, DialogCoordinator.Instance, notificationManagerMock.Object);

            //Act
            mapViewModel.DeleteAllLogosInCityCommand.Execute(null);

            //Assert
            graphicItemsManagerMock.Verify(x => x.DeleteAllLogosAtCity(mapViewModel.LogoViewModel.City), Times.Once);
        }

        [TestMethod]
        public void DeleteAllLogosInCity_Execute_NotificationWasShowedOnes()
        {
            //Arrange
            var graphicItemsManagerMock = new Mock<IGraphicItemsManager>();
            var randomCoordinateGeneratorMock = new Mock<IRandomCoordinatesGenerator>();
            var notificationManagerMock = new Mock<INotificationManager>();

            var mapViewModel = new MapViewModel(randomCoordinateGeneratorMock.Object, graphicItemsManagerMock.Object, DialogCoordinator.Instance, notificationManagerMock.Object);

            //Act
            mapViewModel.DeleteAllLogosInCityCommand.Execute(null);

            //Assert
            notificationManagerMock.Verify(x => x.Show(It.IsAny<NotificationContent>(),
                It.IsAny<string>(),
                It.IsAny<TimeSpan>(), null, null), Times.Once);
        }

        //Refactor using Moq
        [TestMethod]
        public void AddFeatureCommand_Execute_graphicItemManagerWithProperParameterCalled()
        {
            //Arrange
            var notificationManagerMock = new Mock<INotificationManager>();
            List<MapPoint> receivedPoints = null;
            var city = new City { Location = new CityRectangleLocation(1, 2, 1, 2), Name = "City" };
            var randomCoordinatesGeneratorMock = new Mock<IRandomCoordinatesGenerator>();
            randomCoordinatesGeneratorMock.Setup(x => x.GenerateForCity(It.IsAny<City>(), It.IsAny<int>()))
                .Returns(new List<MapPoint> { new MapPoint(0, 0) });

            var generateForCityResult = randomCoordinatesGeneratorMock.Object.GenerateForCity(null, 5);

            var graphicManagerMock = new Mock<IGraphicItemsManager>();

            graphicManagerMock.Setup(x => x.PlotCityPoints(It.IsAny<City>()))
                .Callback(() =>
                {
                    receivedPoints = generateForCityResult;
                });

            var mapViewmodel = new MapViewModel(randomCoordinatesGeneratorMock.Object, graphicManagerMock.Object, DialogCoordinator.Instance, notificationManagerMock.Object);

            //Act
            mapViewmodel.AddFeatureCommand.Execute(null);

            //Assert
            Assert.AreEqual(generateForCityResult, receivedPoints,
                "First param of the 'PlotCityPoints' func was not generated from random coordinates generator.");
            randomCoordinatesGeneratorMock.Verify(x => x.GenerateForCity(It.IsAny<City>(), It.IsAny<int>()), Times.AtLeastOnce);
            graphicManagerMock.Verify(x => x.PlotCityPoints(It.IsAny<City>()),Times.Once);
            graphicManagerMock.Verify(x => x.AddOrUpdateTextLabelForCity(It.IsAny<City>(),It.IsAny<int>()),Times.Once);
        }

        [TestMethod]
        public void AddFeatureCommand_Execute_NotificationWasShowed()
        {
            //Arrange
            var graphicItemsManagerMock = new Mock<IGraphicItemsManager>();
            var randomCoordinateGeneratorMock = new Mock<IRandomCoordinatesGenerator>();
            var notificationManagerMock = new Mock<INotificationManager>();
            randomCoordinateGeneratorMock.Setup(x => x.GenerateForCity(It.IsAny<City>(), It.IsAny<int>()))
                .Returns(new List<MapPoint> { null });

            var mapViewModel = new MapViewModel(randomCoordinateGeneratorMock.Object, graphicItemsManagerMock.Object, DialogCoordinator.Instance, notificationManagerMock.Object);

            //Act
            mapViewModel.AddFeatureCommand.Execute(null);

            //Assert
            notificationManagerMock.Verify(x => x.Show(It.IsAny<NotificationContent>(),
                It.IsAny<string>(),
                It.IsAny<TimeSpan>(), null, null), Times.Once);
        }


        [TestMethod]
        public void ChangedBaseMapCommand_Execute_BaseMapInstanceEqualToBasemap()
        {
            //Arrange
            var graphicItemsManagerMock = new Mock<IGraphicItemsManager>();
            var randomCoordinateGeneratorMock = new Mock<IRandomCoordinatesGenerator>();
            var notificationManagerMock = new Mock<INotificationManager>();

            var mapViewModel = new MapViewModel(randomCoordinateGeneratorMock.Object, graphicItemsManagerMock.Object, DialogCoordinator.Instance, notificationManagerMock.Object);

            //Act
            BaseMapBase newMap = new MapBaseMapFake();
            mapViewModel.ChangeBaseMapCommand.Execute(newMap);

            //Assert
            Assert.AreEqual(newMap.BaseMapInstance, mapViewModel.Map.Basemap);
        }

        [TestMethod]
        public void ZoomToLocationCommand_Execute_CurrentViewPointEqualToViewPoint()
        {
            //Arrange
            var graphicItemsManagerMock = new Mock<IGraphicItemsManager>();
            var randomCoordinateGeneratorMock = new Mock<IRandomCoordinatesGenerator>();
            var notificationManagerMock = new Mock<INotificationManager>();

            var mapViewModel = new MapViewModel(randomCoordinateGeneratorMock.Object, graphicItemsManagerMock.Object, DialogCoordinator.Instance, notificationManagerMock.Object);

            //Act
            mapViewModel.ZoomToLocationCommand.Execute(null); 

            //Assert
            Assert.AreEqual(mapViewModel.ViewPointViewModel.CurrentLongitudeValue, mapViewModel.CurrentViewPoint.TargetGeometry.Extent.GetCenter().X);
            Assert.AreEqual(mapViewModel.ViewPointViewModel.CurrentLatitudeValue, mapViewModel.CurrentViewPoint.TargetGeometry.Extent.GetCenter().Y);
            Assert.AreEqual(mapViewModel.ViewPointViewModel.CurrentScaleValue, mapViewModel.CurrentViewPoint.TargetScale);
        }

        [TestMethod]
        public void ZoomToCityCommand_Execute_AreEqualCurrentViewPointAndCityLocation()
        {
            //Arrange
            var graphicItemsManagerMock = new Mock<IGraphicItemsManager>();
            var randomCoordinateGeneratorMock = new Mock<IRandomCoordinatesGenerator>();
            var notificationManagerMock = new Mock<INotificationManager>();

            var mapViewModel = new MapViewModel(randomCoordinateGeneratorMock.Object, graphicItemsManagerMock.Object, DialogCoordinator.Instance, notificationManagerMock.Object);

            //Act
            var city = new City {Location = new CityRectangleLocation(1, 2, 1, 2),/*CountOfFeatures = 1,*/Name = "City"};
            mapViewModel.ZoomToCityCommand.Execute(city);

            //Assert
            Assert.AreEqual(city.Location.GetCityCoordinates(), mapViewModel.CurrentViewPoint); 
            Assert.AreEqual(city, mapViewModel.LogoViewModel.City);
        }

        [TestMethod]
        public void ShowDialogAndDeleteSelectedLogoCommand_Execute_AffirmativeAndNegativeDialogWasShown()
        {
            MessageDialogStyle? messageDialogStyle = null;

            //Arrange
            var graphicItemsManagerMock = new Mock<IGraphicItemsManager>();
            var randomCoordinateGeneratorMock = new Mock<IRandomCoordinatesGenerator>();
            var notificationManagerMock = new Mock<INotificationManager>();
            var dialogCoordinatorMock = new Mock<IDialogCoordinator>();

            dialogCoordinatorMock.Setup(x => x.ShowMessageAsync(It.IsAny<object>(), It.IsAny<string>(), 
                It.IsAny<string>(), It.IsAny<MessageDialogStyle>(),null))
                .Callback<object,string, string,
                    MessageDialogStyle, MetroDialogSettings>(((o, s, arg3, style, d) => messageDialogStyle = style));

            var mapViewModel = new MapViewModel(randomCoordinateGeneratorMock.Object, graphicItemsManagerMock.Object, dialogCoordinatorMock.Object, notificationManagerMock.Object);

            //Act
            mapViewModel.ShowMapLogoRemovalConfirmationCommand.Execute(null);

            //Assert
            dialogCoordinatorMock.Verify(x => x.ShowMessageAsync(It.IsAny<object>(), It.IsAny<string>(),
                It.IsAny<string>(), It.IsAny<MessageDialogStyle>(), null), Times.Once);
            Assert.AreEqual(messageDialogStyle.Value, MessageDialogStyle.AffirmativeAndNegative);
        }

        [TestMethod]
        public void ShowDialogAndDeleteSelectedLogoCommand_Execute_SelectedLogoDeletedAndNotificated()
        {
            //Arrange
            var graphicItemsManagerMock = new Mock<IGraphicItemsManager>();
            var randomCoordinateGeneratorMock = new Mock<IRandomCoordinatesGenerator>();
            var notificationManagerMock = new Mock<INotificationManager>();
            var dialogCoordinatorMock = new Mock<IDialogCoordinator>();

            dialogCoordinatorMock.Setup(x => x.ShowMessageAsync(It.IsAny<object>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<MessageDialogStyle>(), null))
                .ReturnsAsync(MessageDialogResult.Affirmative);

            var mapViewModel = new MapViewModel(randomCoordinateGeneratorMock.Object, graphicItemsManagerMock.Object, dialogCoordinatorMock.Object, notificationManagerMock.Object);

            //Act
            mapViewModel.ShowMapLogoRemovalConfirmationCommand.Execute(null);

            //Assert
            graphicItemsManagerMock.Verify(x => x.DeleteSelectedLogo(),Times.Once);
            notificationManagerMock.Verify(x => x.Show(It.IsAny<NotificationContent>(),
                It.IsAny<string>(),
                It.IsAny<TimeSpan>(), null, null), Times.Once);
        }

        [TestMethod]
        public void ShowDialogAndDeleteSelectedLogoCommand_Execute_UpdateTextLabelForCityCalled()
        {
            //Arrange
            var graphicItemsManagerMock = new Mock<IGraphicItemsManager>();
            var randomCoordinateGeneratorMock = new Mock<IRandomCoordinatesGenerator>();
            var notificationManagerMock = new Mock<INotificationManager>();
            var dialogCoordinatorMock = new Mock<IDialogCoordinator>();

            dialogCoordinatorMock.Setup(x => x.ShowMessageAsync(It.IsAny<object>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<MessageDialogStyle>(), null))
                .ReturnsAsync(MessageDialogResult.Affirmative);

            var mapViewModel = new MapViewModel(randomCoordinateGeneratorMock.Object, graphicItemsManagerMock.Object, dialogCoordinatorMock.Object, notificationManagerMock.Object);
            var city = mapViewModel.CityList[0];
            city.Graphics.Add(new Graphic());
            city.Graphics[0].IsSelected = true;

            //Act
            mapViewModel.ShowMapLogoRemovalConfirmationCommand.Execute(null);

            //Assert
            graphicItemsManagerMock.Verify(x => x.AddOrUpdateTextLabelForCity(city,city.Graphics.Count), Times.AtLeastOnce);
            Assert.AreEqual(city.Graphics.Count,0);
        }

        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext)
        {
			// This scheme is registered in Application static constructor.
			// We need to make sure that this URI scheme is registered in order to avoid UriFormatExceptions
			var current = Application.Current;
		}
    }
}
