﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media.Imaging;
using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Mapping;
using Esri.ArcGISRuntime.Symbology;
using Esri.ArcGISRuntime.UI;
using Esri.ArcGISRuntime.UI.Controls;
using Map.Models;
using Map.Models.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Map.UnitTests
{
    [TestClass]
    public class GraphicItemsManagerTests
    {
        [TestMethod]
        public void PlotCityPoints_AddLogos_graphicsOverlayForLogoHasLogos()
        {
            //Arrange
            var graphicsOverlayForLogo = new GraphicsOverlay();
            var graphicsOverlayForText = new GraphicsOverlay();
            var graphicItemManager = new GraphicItemsManager(graphicsOverlayForLogo, graphicsOverlayForText);

            var graphics = new List<Graphic> {new Graphic()};
            var city = new City {Location = new CityRectangleLocation(1, 2, 1, 2), Name = "City", Graphics = graphics};

            //Act
            graphicItemManager.PlotCityPoints(city);

            //Assert
            Assert.AreEqual(graphics.Count, graphicsOverlayForLogo.Graphics.Count);
        }

        [TestMethod]
        public void AddOrUpdateTextLabelForCity_AddOrUpdateTextLabelForCity_graphicsOverlayForTextHasTextLabel()
        {
            var graphicsOverlayForLogo = new GraphicsOverlay();
            var graphicsOverlayForText = new GraphicsOverlay();
            var graphicItemManager = new GraphicItemsManager(graphicsOverlayForLogo, graphicsOverlayForText);
            City city = new City
            {
                Location = new CityRectangleLocation(1, 2, 1, 2),
                Name = "City",
                Graphics = new List<Graphic>{new Graphic()}
            };

            //Act
            graphicItemManager.AddOrUpdateTextLabelForCity(city, 1);

            //Assert
            var tes = graphicsOverlayForText.Graphics.First();
            Assert.IsTrue(graphicsOverlayForText.Graphics.Count > 0);
            Assert.AreEqual(city.Location.GetLatitude, tes.Geometry.Extent.YMax);
            Assert.AreEqual(city.Location.GetLongitude, tes.Geometry.Extent.XMax);
        }

        [TestMethod]
        public void DeleteAllLogos_DeleteAllFeaturesAndCallout_ReturnZeroFeaturesForBothGraphicsOverlaysAndRaiseEvent()
        {
            
            //Arrange
            var graphicsOverlayForLogo = new GraphicsOverlay();
            var graphicsOverlayForText = new GraphicsOverlay();
            var graphicItemManager = new GraphicItemsManager(graphicsOverlayForLogo, graphicsOverlayForText);
            
            City city = new City
            {
                Location = new CityRectangleLocation(1, 2, 1, 2),
                Name = "City",
                Graphics = new List<Graphic> { new Graphic() }
            };

            bool deleteCalloutEventCalled = false;

            graphicItemManager.DeleteCalloutFromMap += (object sender, EventArgs args) =>
            {
                deleteCalloutEventCalled = true;
            };

            graphicItemManager.PlotCityPoints(city); 
            graphicItemManager.AddOrUpdateTextLabelForCity(city, 1);
           

            //Act
            graphicItemManager.DeleteAllLogos();

            //Assert
            Assert.AreEqual(0, graphicsOverlayForLogo.Graphics.Count);
            Assert.AreEqual(0, graphicsOverlayForText.Graphics.Count);
            Assert.IsTrue(deleteCalloutEventCalled);
        }

        [TestMethod]
        public void DeleteAllLogosAtCity_DeleteAllLogosAtCity_graphicsOverlayForLogoHasNoLogosAtCity()
        {
            bool deleteCalloutEventCalled = false;
            
            //Arrange
            var graphicsOverlayForLogo = new GraphicsOverlay();
            var graphicsOverlayForText = new GraphicsOverlay();
            var graphicItemManager = new GraphicItemsManager(graphicsOverlayForLogo, graphicsOverlayForText);

            City city = new City
            {
                Location = new CityRectangleLocation(1, 2, 1, 2),
                Name = "City",
                Graphics = new List<Graphic> { new Graphic() }
            };

            graphicItemManager.DeleteCalloutFromMap += (object sender, EventArgs args) =>
            {
                deleteCalloutEventCalled = true;
            };
            //Act
            graphicItemManager.PlotCityPoints(city);
            graphicItemManager.DeleteAllLogosAtCity(city);

            var res = city.Graphics;
            //Assert
            Assert.IsFalse(graphicsOverlayForLogo.Graphics.Contains(res.FirstOrDefault()));
            Assert.IsTrue(deleteCalloutEventCalled);
        }

        [TestMethod]
        public void IsMapHasLogos_MapWithLogos_ReturnTrue()
        {
            //Arrange
            var graphicsOverlayForLogo = new GraphicsOverlay();
            var graphicsOverlayForText = new GraphicsOverlay();
            var graphicItemManager = new GraphicItemsManager(graphicsOverlayForLogo, graphicsOverlayForText);

            //Act
            graphicsOverlayForLogo.Graphics.Add(new Graphic());
            graphicsOverlayForText.Graphics.Add(new Graphic());

            var result = graphicItemManager.IsMapHasLogos();

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsMapHasLogos_MapWithoutLogos_ReturnFalse()
        {
            //Arrange
            var graphicsOverlayForLogo = new GraphicsOverlay();
            var graphicsOverlayForText = new GraphicsOverlay();
            var graphicItemManager = new GraphicItemsManager(graphicsOverlayForLogo, graphicsOverlayForText);

            //Act
            var result = graphicItemManager.IsMapHasLogos();

            //Assert
            Assert.IsFalse(result);
        }

        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext)
        {
            // This scheme is registered in Application static constructor.
            // We need to make sure that this URI scheme is registered in order to avoid UriFormatExceptions
            var current = Application.Current;
        }
    }
}
