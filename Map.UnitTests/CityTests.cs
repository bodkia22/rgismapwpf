﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Symbology;
using Esri.ArcGISRuntime.UI;
using Map.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Map.UnitTests
{
    [TestClass]
    public class CityTests
    {
        [TestMethod]
        public void City_UpdateCityGraphics_CityHasGraphics()
        {
            //Arrange
            var listMapPoint = new List<MapPoint> {new MapPoint(1, 1)};
            var logo = new FeatureLogo
            {
                BitmapImage =
                    new BitmapImage(new Uri("pack://application:,,,/Infrastructure;component/Images/llama.png")),
                Descriptions = "Description"
            };

            City city = new City
            {
                Location = new CityRectangleLocation(1, 2, 1, 2),
                Name = "City"
            };
            var picMarkerSym = new PictureMarkerSymbol(logo.BitmapImage.UriSource);

            //Act
            city.UpdateCityGraphics(listMapPoint, logo);

            var symbols = city.Graphics.Select(x => (PictureMarkerSymbol)x.Symbol).ToList();
            
            //assert
            Assert.AreEqual(listMapPoint.Count, city.Graphics.Count);
            Assert.IsTrue(symbols.All(x => x.Uri == picMarkerSym.Uri));
        }

        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext)
        {
            // This scheme is registered in Application static constructor.
            // We need to make sure that this URI scheme is registered in order to avoid UriFormatExceptions
            var current = Application.Current;
        }
    }
}
