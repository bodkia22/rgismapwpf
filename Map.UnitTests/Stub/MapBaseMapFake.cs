﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Esri.ArcGISRuntime.Mapping;
using Map.Models.Abstractions;

namespace Map.UnitTests.Stub
{
    public class MapBaseMapFake : BaseMapBase
    {
        public MapBaseMapFake()
        {
            BaseMapInstance = new Basemap();
        }
    }
}
