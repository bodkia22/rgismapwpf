﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Esri.ArcGISRuntime.Geometry;
using Map.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Map.UnitTests
{
    [TestClass]
    public class CityRectangleLocationTests
    {
        [TestMethod]
        public void GetCenterPoint_GetCenterPoint_ReturnProperCenterPoint()
        {
            //Arrange
            var cityRectangleLocation = new CityRectangleLocation(0, 1, 0, 1);
            var expectedPoint = new MapPoint(0.5, 0.5, SpatialReferences.Wgs84);

            //Act
            var result = cityRectangleLocation.GetCenterPoint();

            //Assert
            Assert.AreEqual(expectedPoint.X, result.X);
            Assert.AreEqual(expectedPoint.Y, result.Y);
            Assert.AreEqual(expectedPoint.Z, result.Z);
        }
    }
}
